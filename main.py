from scapy.all import *
import sys
from threading import Thread
from scapy.layers.l2 import ARP
from uuid import getnode as get_mac


self_mac = get_mac()
self_mac = ':'.join(("%012X" % self_mac)[i:i+2] for i in range(0, 12, 2))

op = 2
mac = raw_input("Enter client mac addres: ").strip()
mac = mac.replace("-", ":")
mac = mac.replace(" ", "")

sock = socket.socket()
sock.bind(('127.0.0.1', 9090))
sock.listen(1)
conn, addr = sock.accept()

s = socket.socket(socket.PF_PACKET, socket.SOCK_RAW, socket.IPPROTO_RAW)
s.bind((raw_input("Input interface: ").strip(), socket.htons(0x0800)))


def client():
    while 1:
        cupcher = sniff(filter='arp', count=1)
        if cupcher[0].hwdst.upper() == self_mac:
            sys.stdout.write(''.join([chr(int(i)) for i in cupcher[0].psrc.split('.')]))
            sys.stdout.write(''.join([chr(int(i)) for i in cupcher[0].pdst.split('.')]))
            s = cupcher[0].hwsrc.replace(":", "")
            # Decode string
            sys.stdout.write(''.join([chr(int(s[i:i + 2], 16)) for i in range(0, len(s), 2)]))


def server():
    while 1:
        data = str(conn.recv(14))
        while len(data) < 14:
            data += '\x00'
        buf = ''
        for i in range(4):
            buf += str(ord(data[i])) + '.'
        field1 = buf[:len(buf) - 1]
        buf = ''
        for i in range(4, 8):
            buf += str(ord(data[i])) + '.'
        firld2 = buf[:len(buf) - 1]
        buf = ''
        for i in range(8, 14):
            buf1 = hex(ord(data[i]))[2:] + ':'
            buf += buf1 if len(buf1) == 3 else ('0' + buf1)
        data = buf[:len(buf) - 1]
        print(field1, firld2, data)
        arp = ARP(op=op, psrc=field1, pdst=firld2, hwdst=mac, hwsrc=data)
        nmac = ''.join([chr(int(i, 16)) for i in mac.split(':')])
        nself_mac = ''.join([chr(int(i, 16)) for i in mac.split(':')])
        s.send(nmac + nself_mac + '\x08\x06' + str(arp))


if __name__ == '__main__':
    t1 = Thread(target=client)
    t2 = Thread(target=server)

    t1.start()
    t2.start()

    t1.join()
    t2.join()
